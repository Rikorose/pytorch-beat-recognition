#!/usr/bin/env python3

import torch.nn as nn


class SequenceWise(nn.Module):

    def __init__(self, module):
        """
        Collapses input of dim T*N*H to (T*N)*H, and applies to a module.
        Allows handling of variable sequence lengths and minibatch sizes.
        :param module: Module to apply input to.
        """
        super(SequenceWise, self).__init__()
        self.module = module

    def forward(self, x):
        t, n = x.size(0), x.size(1)
        x = x.view(t * n, -1)
        x = self.module(x)
        x = x.view(t, n, -1)
        return x

    def __repr__(self):
        tmpstr = self.__class__.__name__ + ' (\n'
        tmpstr += self.module.__repr__()
        tmpstr += ')'
        return tmpstr


DefaultModelOpts = {
    'input_channels': 3,
    'input_height': 64,
    'conv_out_channels_1': 16,
    'conv_out_channels_2': 32,
    'conv_kernel_size': (5, 5),
    'rnn_hidden_units': 128,
    'rnn_hidden_layers': 5,
    'rnn_type': 'gru',
    'rnn_bidirectional': False,
}


class Model(nn.Module):
    supported_rnns = {'lstm': nn.LSTM, 'rnn': nn.RNN, 'gru': nn.GRU}

    def __init__(self, opts: dict = DefaultModelOpts):
        super().__init__()
        assert opts['rnn_type'] in self.supported_rnns

        self._opts = opts
        self._n_directions = int(self._opts["rnn_bidirectional"]) + 1

        if isinstance(opts['conv_kernel_size'], int):
            padding = opts['conv_kernel_size'] // 2
        else:
            padding = tuple(s // 2 for s in opts['conv_kernel_size'])
        self.conv = nn.Sequential(
            nn.Conv2d(opts['input_channels'], opts['conv_out_channels_1'],
                      kernel_size=opts['conv_kernel_size'], padding=padding),
            # nn.SELU(),
            nn.BatchNorm2d(opts['conv_out_channels_1']),
            # nn.Hardtanh(0, 20, inplace=True),
            nn.ReLU(inplace=True),
            nn.Conv2d(opts['conv_out_channels_1'], opts['conv_out_channels_2'],
                      kernel_size=opts['conv_kernel_size'], padding=padding),
            # nn.SELU(),
            nn.BatchNorm2d(opts['conv_out_channels_2']),
            # nn.Hardtanh(0, 20, inplace=True),
            nn.ReLU(inplace=True),
        )
        self.batch_norm = SequenceWise(nn.BatchNorm1d(
            opts['input_height'] * opts['conv_out_channels_2']))
        self.rnn = self.supported_rnns[opts['rnn_type']](
            input_size=opts['input_height'] * opts['conv_out_channels_2'],
            hidden_size=opts['rnn_hidden_units'],
            num_layers=opts['rnn_hidden_layers'],
            bidirectional=opts['rnn_bidirectional'],
            bias=False,
        )
        fully_connected = nn.Sequential(
            # nn.SELU(),
            nn.BatchNorm1d(opts['rnn_hidden_units']),
            nn.Linear(opts['rnn_hidden_units'], 2, bias=False),
        )
        self.fc = nn.Sequential(SequenceWise(fully_connected),)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x, hidden):
        if len(x.size()) == 3:
            x = x.view(1, x.size(0), x.size(1), x.size(2))
        x = self.conv(x)
        x = x.view(x.size(0),
                   x.size(1) * x.size(2), x.size(3))  # NxCxHxT -> NxH'xT
        x = x.transpose(1, 2).transpose(0, 1).contiguous()  # NxHxT -> TxNxH
        x = self.batch_norm(x)
        x, hidden = self.rnn(x, hidden)
        if self._opts["rnn_bidirectional"]:
            x = x.view(x.size(0), x.size(1), 2, -1).sum(2).view(
                x.size(0), x.size(1), -1)  # (TxNxH*2) -> (TxNxH) by sum
        x = self.fc(x)
        x = x.transpose(0, 1)
        if not self.training:
            x = self.softmax(x)
        return x, hidden

    def init_hidden(self, batch_size):
        weight = next(self.parameters()).data
        if self._opts["rnn_type"] == 'LSTM':
            return (
                weight.new(
                    self._opts["rnn_hidden_layers"] * self._n_directions,
                    batch_size,
                    self._opts["rnn_hidden_units"]
                ).zero_(),
                weight.new(
                    self._opts["rnn_hidden_layers"] * self._n_directions,
                    batch_size,
                    self._opts["rnn_hidden_units"]
                ).zero_()
            )
        else:
            return weight.new(
                self._opts["rnn_hidden_layers"] * self._n_directions,
                batch_size,
                self._opts["rnn_hidden_units"]
            ).zero_()

    def model_opts(self):
        return self._opts
