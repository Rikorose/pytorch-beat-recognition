#!/usr/bin/env python3

import argparse
import time
import logging
import json
import copy
import os
import glob

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.onnx

from rnn_model import Model, DefaultModelOpts
from audiodataset import BallroomDataset, SubsequenceSampler, \
    DefaultSpecDatasetOps
import metrics

parser = argparse.ArgumentParser()

parser.add_argument('-d, '
                    '--debug', dest='debug', action='store_true',
                    help='Print additional training and model information')

parser.add_argument('--data_dir', type=str, default='/tmp/data',
                    help='The path to the dataset directory.')

parser.add_argument('--model_dir', type=str, default='/tmp/model',
                    help='The directory where the model will be stored.')

parser.add_argument('--checkpoint_dir', type=str, default='/tmp/model',
                    help='The directory where the checkpoints will be stored.')

parser.add_argument('--train_epochs', type=int, default=500,
                    help='The number of epochs to train.')

parser.add_argument('--epochs_per_eval', type=int, default=5,
                    help='The number of batches to run in between evaluations.')

parser.add_argument('--batch_size', type=int, default=16,
                    help='The number of images per batch.')

parser.add_argument('--sequence_len', type=int, default=2000,
                    help='Sequence length in number of samples a 10 ms.')

parser.add_argument('--bptt', type=int, default=50,
                    help='Sequence length for backpropagation through time.')

parser.add_argument('--clip', type=float, default=0.25,
                    help='Gradinent clipping for rnn training.')

parser.add_argument('--num_workers', type=int, default=4,
                    help='Number of workers used in data-loading')

parser.add_argument('--no_cuda', dest='cuda', action='store_false',
                    help='Do not use cuda to train model.')

parser.add_argument('--data_parallel', dest='data_parallel',
                    action='store_true',
                    help='Use nn.DataParallel for distributed training with '
                    'multiple GPUs.')

parser.add_argument('--lr', '--learning_rate', type=float, default=1e-4,
                    help='Initial learning rate. Will get multiplied by the '
                    'batch size.')

parser.add_argument('--momentum', type=float, default=0.9, help='momentum')

parser.add_argument('--num_mels', type=int, default=64,
                    help='Number of Mel-frequency cepstrum coefficients for '
                    'the feature computation.')

parser.add_argument('--n_fft', nargs='*',
                    help='FFT size. Multiple sizes can be specified and will '
                    'all be computed as input.')

parser.add_argument('--conv_out_channels_1', type=int, default=16,
                    help='Number of output channels for the first conv layer.')

parser.add_argument('--conv_out_channels_2', type=int, default=32,
                    help='Number of output channels for the second conv layer.')

parser.add_argument('--conv_kernel_size', type=int, default=5,
                    help='Convolution kernel size.')

parser.add_argument('--rnn_type', type=str, default='gru',
                    help='Type of the RNN (rnn|gru|lstm are supported).')

parser.add_argument('--rnn_hidden_layers', type=int, default=5,
                    help='Number of RNN layers.')

parser.add_argument('--rnn_hidden_units', type=int, default=128,
                    help='Hidden size of RNNs.')

parser.add_argument('--rnn_bidirectional', dest='rnn_bidirectional',
                    action='store_true', help='Train the RNN bidirectional.')

parser.add_argument('--out_hidden_size', type=int, default=128,
                    help='Hidden size of the output layer.')

ARGS = parser.parse_args()

_logger = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.INFO if not ARGS.debug else logging.DEBUG,
    format='%(asctime)s|%(levelname)s|%(message)s',
    datefmt='%H:%M:%S',
)


def log_debug(msg, *args, **kwargs):
    _logger.debug(msg, *args, **kwargs)


def log_info(msg, *args, **kwargs):
    _logger.info(msg, *args, **kwargs)


def log_warning(msg, *args, **kwargs):
    _logger.warning(msg, *args, **kwargs)


def log_error(msg, *args, **kwargs):
    _logger.error(msg, *args, **kwargs)


def epoch_log(phase, epoch, num_epochs, epoch_time, loss, accuracy, f1_score,
              precision, recall, lr):
    _logger.info(
        '{}|{:03d}/{:d}|loss:{:0.3f}|acc:{:0.3f}|f1:{:0.3f}|'
        'pr:{:0.3f}|re:{:0.3f}|lr:{:0.2e}|t:{:0.1f}'.format(
            phase.upper().rjust(5, ' '),
            epoch,
            num_epochs,
            loss,
            accuracy,
            f1_score,
            precision,
            recall,
            lr,
            epoch_time,
        ),
    )


def write_np_to_file(file, x):
    np.savetxt(file, x, fmt='%.2f')


DefaultTrainOpts = {
    'lr': 3e-4,
    'momentum': 0.9,
    'clip': 0.25,
    'cuda': False,
    'data_parallel': False,
    'train_epochs': 500,
    'epochs_per_eval': 10,
    'batch_size': 16,
    'sequence_len': 1000,
    'bptt': 50,
}


def write_checkpoint(dir: str, checkpoint_dict: dict, max_checkpoints=5):
    checkpoint_name = os.path.join(dir, "epoch_{:05d}.checkpoint".format(
        checkpoint_dict["trainState"]["epoch"]))
    if not os.path.isdir(dir):
        os.makedirs(dir)
    torch.save(checkpoint_dict, checkpoint_name)
    checkpoints = glob.glob(os.path.join(dir, '*.checkpoint'))
    if len(checkpoints) > max_checkpoints:
        checkpoints.sort()
        for i in range(len(checkpoints) - max_checkpoints):
            os.remove(checkpoints[i])


def read_latest_checkpoint(dir: str):
    checkpoints = glob.glob(os.path.join(dir, '*.checkpoint'))
    if len(checkpoints) == 0:
        return None
    checkpoints.sort()
    log_info("Restoring checkpoint {}".format(checkpoints[-1]))
    return torch.load(checkpoints[-1])


def train_model(model, dataloaders, trainOpts=DefaultTrainOpts,
                checkpoint_dir=None):
    class_weight = torch.Tensor([0.4, 0.6])

    if trainOpts["cuda"]:
        class_weight = class_weight.cuda()
        if trainOpts['data_parallel']:
            model = nn.DataParallel(model).cuda()
        else:
            model = model.cuda()

    best_model_acc = copy.deepcopy(model.state_dict())
    best_accuracy = 0.0
    best_f1 = 0.0

    optimizer = optim.SGD(model.parameters(), lr=trainOpts['lr'],
                          momentum=trainOpts['momentum'])
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.75)

    startEpoch = 0
    # Try to restore a checkpoint if one exists
    if checkpoint_dir is not None:
        checkpoint = read_latest_checkpoint(checkpoint_dir)
        if checkpoint is not None:
            try:
                try:
                    model.load_state_dict(checkpoint['modelState'])
                except RuntimeError:
                    log_error("Failed to restore checkpoint: "
                              "Checkpoint has different parameters")
                    exit(1)
                optimizer.load_state_dict(checkpoint['trainState']['optState'])
                startEpoch = checkpoint['trainState']['epoch']
                best_accuracy = checkpoint['trainState']['best_accuracy']
                best_model_acc = checkpoint['trainState']['best_model_acc']
                scheduler.load_state_dict(
                    checkpoint['trainState']['schedState'])
            except KeyError:
                log_error("Failed to restore checkpoint")
                raise

    criterion = nn.CrossEntropyLoss(class_weight)

    since = time.time()

    epoch_accuracy = metrics.Accuracy()
    epoch_loss = metrics.Mean()
    epoch_f1 = metrics.F1Score()
    epoch_pr = metrics.Precision()
    epoch_re = metrics.Recall()
    data_loading_time = metrics.Sum()

    def run_epoch(phase):
        if phase == 'train':
            scheduler.step()
            model.train()
        else:
            model.eval()

        epoch_accuracy.reset()
        epoch_loss.reset()
        epoch_f1.reset()
        epoch_pr.reset()
        epoch_re.reset()
        data_loading_time.reset()
        epoch_start = time.time()

        for i, (data) in enumerate(dataloaders[phase]):
            batch_start = time.time()
            # get the inputs
            inputs_full, labels_full = data
            hidden = model.init_hidden(inputs_full.size(0))
            for pos in range(trainOpts['sequence_len'] // trainOpts['bptt']):
                pos *= trainOpts['bptt']
                inputs = inputs_full[:, :, :, pos:pos + trainOpts['bptt']]
                labels = labels_full[:, pos:
                                     pos + trainOpts['bptt']].contiguous()
                if trainOpts["cuda"]:
                    inputs = inputs.cuda()
                    labels = labels.cuda(non_blocking=True)
                data_loading_time.update(
                    torch.Tensor([
                        (time.time() - batch_start) / trainOpts['batch_size']
                    ]))

                # Forward
                out, hidden = model(inputs, hidden)
                out = out.reshape(out.size(0) * out.size(1), out.size(2))
                labels = labels.view(labels.size(0) * labels.size(1))
                loss = criterion(out, labels)
                predictions = torch.argmax(out.data, dim=1)

                hidden = hidden.detach()
                if phase == 'train':
                    optimizer.zero_grad()
                    loss.backward()
                    nn.utils.clip_grad_norm_(model.parameters(),
                                             trainOpts['clip'])
                    for p in model.parameters():
                        p.data.add_(-scheduler.get_lr()[0], p.grad.data)
                    optimizer.step()
                else:
                    if i % 50 == 0:
                        write_np_to_file(
                            'out',
                            np.concatenate([
                                np.expand_dims(labels[100:300].cpu().numpy(),
                                               axis=1),
                                out[100:300].detach().cpu().numpy()
                            ], axis=1))

                # statistics
                epoch_loss.update(loss.data)
                epoch_accuracy.update(labels.data, predictions.data)
                epoch_f1.update(labels.data, predictions.data)
                epoch_pr.update(labels.data, predictions.data)
                epoch_re.update(labels.data, predictions.data)

                del loss, out
                del inputs, labels

        del inputs_full, labels_full, data

        epoch_time = time.time() - epoch_start
        epoch_log(phase, epoch, trainOpts['train_epochs'] - 1, epoch_time,
                  epoch_loss, epoch_accuracy, epoch_f1, epoch_pr, epoch_re,
                  scheduler.get_lr()[0])
        log_debug('Dataloading time: {:.3e}'.format(data_loading_time.get()))

    try:
        for epoch in range(startEpoch, trainOpts['train_epochs']):
            run_epoch('train')
            if epoch % trainOpts['epochs_per_eval'] == 0 or \
                    epoch == trainOpts['train_epochs'] - 1:
                run_epoch('val')
                # deep copy the model
                if epoch_accuracy.get() > best_accuracy:
                    best_accuracy = epoch_accuracy.get()
                    best_model_acc = copy.deepcopy(model.state_dict())
                if epoch_f1.get() > best_f1:
                    best_f1 = epoch_f1.get()
                    # TODO
                checkpoint_dict = {
                    'modelOpts': model.model_opts(),
                    'modelState': model.state_dict(),
                    'trainOpts': trainOpts,
                    'trainState': {
                        'epoch': epoch,
                        'best_accuracy': best_accuracy,
                        'best_model_acc': best_model_acc,
                        'optState': optimizer.state_dict(),
                        'schedState': scheduler.state_dict(),
                    },
                }
                write_checkpoint(checkpoint_dir, checkpoint_dict)
    except KeyboardInterrupt:
        log_info('Exiting from training early')
    finally:
        run_epoch('test')

    time_elapsed = time.time() - since
    log_info('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    log_info('Best val acc: {:4f}'.format(best_accuracy))

    # load best model weights
    model.load_state_dict(best_model_acc)
    return model


if __name__ == '__main__':
    if ARGS.n_fft is None:
        ARGS.n_fft = [512, 1024, 2048]
    else:
        ARGS.n_fft = [int(n) for n in ARGS.n_fft]
        log_debug(ARGS.n_fft)
    ARGS.lr *= ARGS.batch_size

    # Initialize options
    modelOpts = DefaultModelOpts
    dataOpts = DefaultSpecDatasetOps
    trainOpts = DefaultTrainOpts

    for arg, value in vars(ARGS).items():
        if arg in modelOpts:
            modelOpts[arg] = value
        if arg in dataOpts:
            dataOpts[arg] = value
        if arg in trainOpts:
            trainOpts[arg] = value
    log_debug('modelOpts: ' + json.dumps(modelOpts, indent=4))
    log_debug('dataOpts: ' + json.dumps(dataOpts, indent=4))
    log_debug('trainOpts: ' + json.dumps(trainOpts, indent=4))

    # Setup dataloaders
    splits = ['train', 'val', 'test']
    sampler = SubsequenceSampler(ARGS.sequence_len, axis=2)
    datasets = {
        s: BallroomDataset(s, ARGS.data_dir, dataOpts, transform=sampler)
        for s in splits
    }
    dataloaders = {
        s:
        torch.utils.data.DataLoader(datasets[s], batch_size=ARGS.batch_size,
                                    shuffle=True, num_workers=ARGS.num_workers)
        for s in splits
    }

    # Initialize model
    model = Model(modelOpts)
    log_debug('Model: ' + str(model))

    # Train model
    model = train_model(model, dataloaders, trainOpts, ARGS.checkpoint_dir)

    # Save model
    model = model.cpu()
    state_dict = model.state_dict()
    if torch.cuda.is_available() and ARGS.cuda and ARGS.data_parallel:
        # Remove module prefix
        from collections import OrderedDict
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            new_state_dict[name] = v
        state_dict = new_state_dict

    save_dict = {
        'modelOpts': modelOpts,
        'dataOpts': dataOpts,
        'trainOpts': trainOpts,
        'modelState': state_dict,
    }
    if not os.path.isdir(ARGS.model_dir):
        os.makedirs(ARGS.model_dir)
    torch.save(save_dict, os.path.join(ARGS.model_dir, "model.pk"))
