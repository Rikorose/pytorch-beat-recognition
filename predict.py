#!/usr/bin/env python3

import argparse
import logging
import json

import torch
import numpy as np

from rnn_model import Model, DefaultModelOpts
from audiodataset import MelSpectrogramDataset


parser = argparse.ArgumentParser()

parser.add_argument('-d, ''--debug', dest='debug', action='store_true',
                    help='Print additional training and model information')

parser.add_argument('--model_file', type=str, default='/tmp/model',
                    help='The directory where the model will be stored.')

parser.add_argument('--cuda', dest='cuda', action='store_true',
                    help='Use cuda to train model.')

parser.add_argument('audio_files', type=str, nargs='+',
                    help='Audio files to predict the beat for.')

ARGS = parser.parse_args()

_logger = logging.getLogger(__name__)

logging.basicConfig(
    level = logging.INFO if not ARGS.debug else logging.DEBUG,
    format = '%(asctime)s|%(levelname)s|%(message)s',
    datefmt = '%H:%M:%S',
)


def debug_log(msg, *args, **kwargs):
    _logger.debug(msg, *args, **kwargs)


def info_log(msg, *args, **kwargs):
    _logger.info(msg, *args, **kwargs)


if __name__ == '__main__':
    save_dict = torch.load(ARGS.model_file)

    state_dict = save_dict['modelState']
    for key in state_dict.keys():
        if 'module.' in key:
            # Remove module prefix
            from collections import OrderedDict
            new_state_dict = OrderedDict()
            for k, v in state_dict.items():
                name = k[7:] # remove `module.`
                new_state_dict[name] = v
            state_dict = new_state_dict
            break

    debug_log('modelOpts: ' + json.dumps(save_dict['modelOpts'], indent=4))
    debug_log('dataOpts: ' + json.dumps(save_dict['dataOpts'], indent=4))

    model = Model(save_dict['modelOpts'])
    model.load_state_dict(state_dict)
    if ARGS.cuda:
        model = model.cuda()
    debug_log('Model: ' + str(model))

    dataset = MelSpectrogramDataset(
        ARGS.audio_files, save_dict['dataOpts'])
    for spec in dataset:
        print("predicting...")
        if ARGS.cuda:
            spec = spec.cuda()
        out = model(spec)
        print(out.size())
        prediction = out.argmax(dim=2).cpu()
        print(prediction.size())
        np.savetxt('pred', prediction.numpy())
