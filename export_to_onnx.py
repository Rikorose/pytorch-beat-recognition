import torch
from torch.autograd import Variable
import torch.onnx

from model import Model

n_mels = 64
sequence_len = 1000
model = Model(
    input_size=n_mels,
    hidden_size=256,
    num_layers=6,
    rnn_type='gru',
    batch_norm=True,
    bidirectional=False,
)

torch.save(model.state_dict(), '/tmp/ballroom_model')
model.load_state_dict(torch.load('/tmp/ballroom_model'))

dummy_input = Variable(torch.randn(1, 3, n_mels, sequence_len))
torch.onnx.export(model, dummy_input, 'model.proro', verbose=False)
