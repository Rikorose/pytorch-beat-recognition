#!/usr/bin/env python3

import os
import glob
import urllib.request
import sys
import math
import random
import csv

import torch
import torch.utils.data
import librosa
import numpy as np


def maybe_download(url, data_dir, force=False):
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    filename = url.split('/')[-1]
    # For Dropbox download
    if filename.endswith('?dl=1'):
        filename = filename[:-5]
    filepath = os.path.join(data_dir, filename)

    if not os.path.exists(filepath) or force:

        def _progress(count, block_size, total_size):
            sys.stdout.write('\rDownloading %s %.1f%%' %
                             (filename,
                              100.0 * count * block_size / total_size))
            sys.stdout.flush()

        filepath, _ = urllib.request.urlretrieve(url, filepath, _progress)
        print()
        statinfo = os.stat(filepath)
        print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')

    if filename.endswith('tar.gz'):
        import tarfile
        print('Extracting...')
        tarfile.open(filepath, 'r:gz').extractall(data_dir)
    elif filename.endswith('.tar'):
        import tarfile
        print('Extracting...')
        tarfile.open(filepath, 'r:').extractall(data_dir)
    elif filename.endswith('.zip'):
        from zipfile import ZipFile
        print('Extracting...')
        ZipFile(filepath, 'r').extractall(data_dir)
    os.remove(filepath)


def train_test_val_split(l, train=.7, test=.15, val=.15, seed=-1):
    if not np.isclose(np.sum([train, test, val]), 1.0):
        raise ValueError(
            'Train, test and validation size have to sum up to 100% (1.0)!')
    if seed >= 0:
        random.seed(seed)
    return np.split(
        random.sample(l, len(l)),
        [int(train * len(l)), int((train + test) * len(l))])


def load_audio_file(file_name, sr=44100, mono=True, offset=0.0):
    y, _ = librosa.load(file_name, sr=sr, mono=mono, offset=offset)
    return y


def compute_spectrogram(file_name, sr, n_fft, hop_length, n_mels, fmin, fmax):
    y = load_audio_file(file_name, sr=sr)
    S = np.empty(
        shape=(len(n_fft), n_mels,
               math.ceil(y.shape[0] / hop_length)), dtype=np.float32)
    for i, n in enumerate(n_fft):
        S[i] = librosa.feature.melspectrogram(
            y, sr, n_fft=n, hop_length=hop_length,
            n_mels=n_mels, fmin=fmin, fmax=fmax)
    return S


class AudioDataset(torch.utils.data.Dataset):

    def __init__(self, data_files=[], transform=None):
        """ Pytorch dataset for loading audio data.

        Args:
            data_dir (str): Path to the directory containing the audio data.
            file_extension (str): Audio file extension, for instance `.mp3`.
        """
        self.data_files = data_files
        self.transform = transform
        super().__init__()

    def load_audio(self, *args, **kwargs):
        return load_audio_file(*args, **kwargs)

    def load_label(self):
        pass

    def __getitem__(self, idx):
        sample = torch.from_numpy(self.load_audio(self.data_files[idx]))
        if self.transform:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return len(self.data_files)


DefaultSpecDatasetOps = {
    'sr': 44100,
    'n_fft': [512, 1024, 2048],
    'hop_length': 441,
    'num_mels': 128,
    'fmin': 30,
    'fmax': 16000,
}


class MelSpectrogramDataset(AudioDataset):

    def __init__(self, data_files=[], opts=DefaultSpecDatasetOps, cache=True,
                 *args, **kwargs):
        """ Pytorch dataset for loading audio data as spectrogram.

        Args:
            data_files (list): List with audio files.
            opts (dict): Dictitonary with spectrogram parameters.
            cache (bool): Cache the computed spectrograms serialized on disk.
        """
        super().__init__(data_files, *args, **kwargs)
        self.sr = opts['sr']
        self.hop_length = opts['hop_length']
        self.n_fft = opts['n_fft']
        if isinstance(self.n_fft, int):
            self.n_fft = [opts['n_fft']]
        self.n_mels = opts['num_mels']
        self.fmin = opts['fmin']
        self.fmax = opts['fmax']
        self.cache = cache
        self.feat_mean = 0
        self.feat_std = 1
        self.prepare_dataset()

    def prepare_dataset(self):
        pass

    def load_audio(self, file_name):
        if self.cache:
            return self._load_from_disk_or_compute(file_name)
        else:
            return self._compute_spectrogram(file_name)

    def _compute_spectrogram(self, file_name):
        return compute_spectrogram(file_name, self.sr, n_fft=self.n_fft,
                                   hop_length=self.hop_length,
                                   n_mels=self.n_mels, fmin=self.fmin,
                                   fmax=self.fmax)

    def _normalize(self, features, eps=1e-14):
        return (features - self.feat_mean) / (self.feat_std + eps)

    def _load_from_disk_or_compute(self, file_name):
        cached_spec = os.path.splitext(file_name)[0] + '.spec'
        if os.path.isfile(cached_spec):
            with open(cached_spec, 'rb') as fh:
                dtype, a1, a2, a3 = fh.readline().decode().split()
                n_fft = fh.readline().decode().split()
                n_mels = int(fh.readline().decode())
                # Check if the cached spectrogram has a correct n_fft
                match = True
                if len(n_fft) != len(self.n_fft):
                    match = False
                if match:
                    for n in n_fft:
                        if int(n) not in self.n_fft:
                            match = False
                            break
                # Also the number of comupted mel coefs should match
                if n_mels != self.n_mels:
                    match = False
                if match:
                    return np.frombuffer(fh.read(), dtype=str(dtype)).reshape(
                        (int(a1), int(a2), int(a3)))
                else:
                    os.remove(cached_spec)
        S = self._compute_spectrogram(file_name)
        S = self._normalize(S)
        try:
            with open(cached_spec, 'wb+') as fh:
                header = '{0:s} {1:d} {2:d} {3:d}\n'.format(
                    str(S.dtype), *S.shape)
                header = header + ' '.join(str(n) for n in self.n_fft) + '\n'
                header = header + str(self.n_mels) + '\n'
                fh.write(header.encode())
                fh.write(S.data)
        except:
            print('Failed to write computed spectrogram to disk: {}'.format(
                cached_spec))
            os.remove(cached_spec)
            raise
        if S.shape[1] != self.n_mels:
            raise ValueError('Spectrogram has wrong n_mels:', S.shape[1])
        return S

    def _compute_mean_std(self, k=0, data_dir='/tmp/data', overwrite=False):
        fn_feat_mean = os.path.join(data_dir, 'feat_mean')
        fn_feat_std = os.path.join(data_dir, 'feat_std')
        if not overwrite and os.path.isfile(fn_feat_mean) and os.path.isfile(
                fn_feat_std):
            self.feat_mean = np.loadtxt(fn_feat_mean)
            self.feat_std = np.loadtxt(fn_feat_std)
        else:
            if k == 0:
                # Compute spectrograms for all train samples
                k = self.__len__()
                samples = [s for s, _ in self.train_files]
            else:
                samples = [s for s, _ in random.sample(self.train_files, k)]

            feats = np.empty((len(self.n_fft), self.n_mels, k))
            for i, sample in enumerate(samples):
                S = self.load_audio(sample)
                feats[:, :, i] = np.mean(S, axis=2)
            feats = np.sum(feats, axis=2)
            self.feat_mean = feats / k
            self.feat_std = np.sqrt(np.square(feats - self.feat_mean) / k)
            np.savetxt(os.path.join(data_dir, 'feat_mean'), self.feat_mean)
            np.savetxt(os.path.join(data_dir, 'feat_std'), self.feat_std)


class BallroomDataset(MelSpectrogramDataset):

    def __init__(self, split='train', data_dir='/tmp/ballroom', *args,
                 **kwargs):
        valid_splits = ['train', 'test', 'val']
        if split not in valid_splits:
            raise ValueError('{} is not a valid split. Must be in {}'.format(
                split, valid_splits))
        self.split = split
        self.data_dir = data_dir
        super().__init__(*args, **kwargs)

    def prepare_dataset(self, force=False):
        """Download and extract ballrooms dataset."""
        data_url = 'https://www.dropbox.com/s/rama79nej32uzlc/Ballrooms.zip?dl=1'
        input_file = os.path.join(self.data_dir, 'allBallroomFiles')

        if not os.path.isfile(input_file) or force:
            maybe_download(data_url, self.data_dir, force=force)

        # Generate train, test, and validation splits and save them as csv
        if force or not os.path.isfile(
                os.path.join(self.data_dir, self.split) + '.csv'):
            self.data_files = []
            for line in open(input_file):
                data = line.rstrip('\n').lstrip('./')
                label = os.path.basename(data).replace('.wav', '.beats')
                self.data_files.append(
                    (os.path.join(self.data_dir, 'audio', data),
                     os.path.join(self.data_dir, 'annotations', label)))

            train, test, val = train_test_val_split(self.data_files)

            for n, l in [('train', train), ('test', test), ('val', val)]:
                path = os.path.join(self.data_dir, n) + '.csv'
                with open(path, 'w', newline='') as fh:
                    writer = csv.writer(fh)
                    for row in l:
                        writer.writerow(row)

        self.data_files = []
        with open(os.path.join(self.data_dir, self.split) + '.csv', 'r') as fh:
            reader = csv.reader(fh)
            for row in reader:
                self.data_files.append(tuple(row))

        # Test if load_audio() works, otherwise re-download the data
        try:
            self.load_audio(self.data_files[0][0])
        except FileNotFoundError:
            if not force:  # force now
                self.prepare_dataset(force=True)
            else:
                raise

    def load_label(self, annotation_file, feature_rate=100,
                   feature_length=None, separator=' ', down_beats=False):
        """Load beat labels."""

        if down_beats:
            raise NotImplementedError(
                'Down beat labels are not implemented yet')

        annotations = []
        for line in open(annotation_file):
            annotation = line.rstrip('\n').split(' ')
            if len(annotation) == 1:
                annotation = line.rstrip('\n').split('\t')
            annotations.append((float(annotation[0]), int(annotation[1])))

        if feature_length is None:
            feature_length = int(
                np.ceil(max(annotations, key=lambda a: a[0])[0]) * feature_rate)
        label = np.zeros(feature_length, np.int)
        for a in annotations:
            pos_start = int(a[0] * feature_rate)
            pos = range(pos_start, min(feature_length, pos_start + 5))
            label[pos] = 1
        return label

    def __getitem__(self, idx):
        spectrogram = self.load_audio(self.data_files[idx][0])
        spectrogram = torch.from_numpy(spectrogram)
        labels = self.load_label(self.data_files[idx][1],
                                 feature_length=spectrogram.shape[2],
                                 feature_rate=self.sr / self.hop_length)
        labels = torch.from_numpy(labels)
        if self.transform:
            spectrogram, labels = self.transform((spectrogram, labels))
        return spectrogram, labels


class SubsequenceSampler(object):
    """Samples a subsquence along one axis.

    Args:
        sequence_length (int): Number of samples of the subsequence.
        axis (int): Axis to sample the subsequence from.
    """

    def __init__(self, sequence_length, axis=0):
        assert isinstance(sequence_length, int)
        assert isinstance(axis, int)
        self.sequence_length = sequence_length
        self.axis = axis

    def __call__(self, sample):
        spectrogram, label = sample
        assert (spectrogram.shape[self.axis] == label.shape[0])
        sample_length = spectrogram.shape[self.axis]
        assert (sample_length >= self.sequence_length)
        start = np.random.randint(0, sample_length - self.sequence_length)
        end = start + self.sequence_length
        indices = np.arange(start, end)
        return np.take(spectrogram, indices, axis=self.axis), label[start:end]


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def test_AudioDataset(num_epochs=10):
    test_dir = '/tmp/ballroom'
    sequence_len = 1000
    dataset = BallroomDataset(
        'train',
        data_dir=test_dir,
        n_mels=64,
        n_fft=[512, 1024, 2048, 4096],
        transform=SubsequenceSampler(sequence_len, axis=2),
    )
    batch_size = 16
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=8,
        drop_last=True,
    )

    import time
    from icecream import ic

    def totime(num_epochs=2):
        data_time = AverageMeter()
        for epoch in range(num_epochs):
            end = time.time()
            for i, (data) in enumerate(dataloader):
                data_time.update(time.time() - end)
                print("Data_time per record: {}.".format(
                    data_time.val / batch_size))
                end = time.time()
            print("Avg read time per record: {}".format(
                data_time.avg / batch_size))
            print("Avg read time per batch: {}".format(data_time.avg))
            data_time.reset()
    totime()


if __name__ == '__main__':
    test_AudioDataset()
