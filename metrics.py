import torch


def _safe_div(numerator, denominator):
    """Divides two tensors element-wise, returning 0 if the denominator is <= 0.
    """
    t = torch.div(numerator, denominator)
    condition = torch.gt(denominator, float(0))
    return torch.where(condition, t, torch.zeros_like(t))


class MetricBase():

    def __init__(self):
        pass

    def reset(self):
        self.__init__()

    def update(self):
        pass

    def _get_tensor(self):
        pass

    def get(self):
        return self._get_tensor().item()

    def __str__(self):
        return str(self.get())

    def __format__(self, spec):
        return self.get().__format__(spec)


class Sum(MetricBase):

    def __init__(self):
        self.value = torch.zeros(1)

    def update(self, values, weights=None):
        if not torch.is_tensor(values):
            values = torch.tensor(values, device=self.value.device,
                                  dtype=torch.float)
        elif values.device != self.value.device:
            self.value = torch.tensor(self.value, dtype=torch.float,
                                      device=values.device)
        if weights is not None:
            if torch.is_tensor(weights):
                weights = weights.float()
            values = torch.mul(values, weights)

        self.value += values.sum()

    def _get_tensor(self):
        return self.value

class Max(MetricBase):

    def __init__(self):
        self.value = torch.zeros(1)

    def update(self, values, weights=None):
        if not torch.is_tensor(values):
            values = torch.tensor(values, device=self.value.device,
                                  dtype=torch.float)
        elif values.device != self.value.device:
            self.values = torch.tensor(self.values, dtype=torch.float,
                                      device=values.device)
        if weights is not None:
            if torch.is_tensor(weights):
                weights = weights.float()
            values = torch.mul(values, weights)

        tmp = values.max().float()
        if tmp > self.value:
            self.value = tmp

    def _get_tensor(self):
        return self.value


class Mean(MetricBase):

    def __init__(self, cuda=False):
        self.total = torch.zeros(1, dtype=torch.float)
        self.count = torch.zeros(1, dtype=torch.float)

    def update(self, values, weights=None):
        if not torch.is_tensor(values):
            values = torch.tensor(values, device=self.total.device,
                                  dtype=torch.float)
        elif values.device != self.total.device:
            self.total = torch.tensor(self.total, dtype=torch.float,
                                      device=values.device)
            self.count = torch.tensor(self.count, dtype=torch.float,
                                      device=values.device)
        if weights is None:
            num_values = float(values.numel())
        else:
            if torch.is_tensor(weights):
                num_values = weights.sum().float()
                weights = weights.float()
            else:
                num_values = torch.mul(values.numel(), weights).float()
            values = torch.mul(values, weights)

        self.total += values.sum().float()
        self.count += num_values

    def _get_tensor(self):
        return _safe_div(self.total, self.count).float()


class Accuracy(MetricBase):

    def __init__(self):
        self.mean = Mean()

    def update(self, labels, predictions, weights=None):
        if not torch.is_tensor(labels):
            labels = torch.tensor(labels,
                                  device=self.mean._get_tensor().device,
                                  dtype=torch.float)
        if not torch.is_tensor(predictions):
            predictions = torch.tensor(predictions,
                                  device=labels.device,
                                  dtype=labels.dtype)
        else:
            predictions = predictions.type_as(labels)
        is_correct = torch.eq(labels, predictions).float()
        self.mean.update(is_correct, weights)

    def _get_tensor(self):
        return self.mean._get_tensor()


def _count_condition(condition, weights):
    if weights is not None:
        if torch.is_tensor(weights):
            weights = weights.float()
        condition = torch.mul(condition.float(), weights)
    return condition.sum().item()


class TruePositives(MetricBase):

    def __init__(self):
        self.count = torch.zeros(1, dtype=torch.float)

    def update(self, labels, predictions, weights=None):
        if not torch.is_tensor(labels):
            labels = torch.tensor(labels,
                                  device=self.mean._get_tensor().device,
                                  dtype=torch.float)
        if not torch.is_tensor(labels):
            predictions = torch.tensor(predictions,
                                  device=labels.device,
                                  dtype=labels.dtype)
        else:
            predictions = predictions.type_as(labels)
        is_true = torch.eq(labels, True)
        is_positive = torch.eq(predictions, True)
        condition = torch.mul(is_true, is_positive)
        self.count += _count_condition(condition, weights)

    def _get_tensor(self):
        return self.count


class FalsePositives(MetricBase):

    def __init__(self):
        self.count = torch.zeros(1, dtype=torch.float)

    def update(self, labels, predictions, weights=None):
        if not torch.is_tensor(labels):
            labels = torch.tensor(labels,
                                  device=self.mean._get_tensor().device,
                                  dtype=torch.float)
        if not torch.is_tensor(labels):
            predictions = torch.tensor(predictions,
                                  device=labels.device,
                                  dtype=labels.dtype)
        else:
            predictions = predictions.type_as(labels)
        is_false = torch.eq(labels, False)
        is_positive = torch.eq(predictions, True)
        condition = torch.mul(is_false, is_positive)
        self.count += _count_condition(condition, weights)

    def _get_tensor(self):
        return self.count


class TrueNegatives(MetricBase):

    def __init__(self):
        self.count = torch.zeros(1, dtype=torch.float)

    def update(self, labels, predictions, weights=None):
        if not torch.is_tensor(labels):
            labels = torch.tensor(labels,
                                  device=self.mean._get_tensor().device,
                                  dtype=torch.float)
        if not torch.is_tensor(labels):
            predictions = torch.tensor(predictions,
                                  device=labels.device,
                                  dtype=labels.dtype)
        else:
            predictions = predictions.type_as(labels)
        is_false = torch.eq(labels, False)
        is_negative = torch.eq(predictions, False)
        condition = torch.mul(is_false, is_negative)
        self.count += _count_condition(condition, weights)

    def _get_tensor(self):
        return self.count


class FalseNegatives(MetricBase):

    def __init__(self):
        self.count = torch.zeros(1, dtype=torch.float)

    def update(self, labels, predictions, weights=None):
        if not torch.is_tensor(labels):
            labels = torch.tensor(labels,
                                  device=self.mean._get_tensor().device,
                                  dtype=torch.float)
        if not torch.is_tensor(labels):
            predictions = torch.tensor(predictions,
                                  device=labels.device,
                                  dtype=labels.dtype)
        else:
            predictions = predictions.type_as(labels)
        is_true = torch.eq(labels, True)
        is_negative = torch.eq(predictions, False)
        condition = torch.mul(is_true, is_negative)
        self.count += _count_condition(condition, weights)

    def _get_tensor(self):
        return self.count


class Precision(MetricBase):

    def __init__(self):
        self.tp = TruePositives()
        self.fp = FalsePositives()

    def update(self, labels, predictions, weights=None):
        self.tp.update(labels, predictions, weights)
        self.fp.update(labels, predictions, weights)

    def _get_tensor(self):
        pred_p = self.tp._get_tensor() + self.fp._get_tensor()
        return torch.where(torch.gt(pred_p, 0),
                           torch.div(self.tp._get_tensor(), pred_p),
                           torch.zeros_like(pred_p))


class Recall(MetricBase):

    def __init__(self):
        self.tp = TruePositives()
        self.fn = FalseNegatives()

    def update(self, labels, predictions, weights=None):
        self.tp.update(labels, predictions, weights)
        self.fn.update(labels, predictions, weights)

    def _get_tensor(self):
        p = self.tp._get_tensor() + self.fn._get_tensor()
        return torch.where(torch.gt(p, 0),
                           torch.div(self.tp._get_tensor(), p),
                           torch.zeros_like(p))


class F1Score(MetricBase):

    def __init__(self):
        self.pr = Precision()
        self.re = Recall()

    def update(self, labels, predictions, weights=None):
        self.pr.update(labels, predictions, weights)
        self.re.update(labels, predictions, weights)

    def _get_tensor(self):
        s = self.pr._get_tensor() + self.re._get_tensor()
        return torch.where(
            torch.gt(s, 0),
            torch.div(2 * self.pr._get_tensor() * self.re._get_tensor(), s),
            torch.zeros_like(s))


def test_Mean(gpu=False):
    import numpy as np
    x = np.random.random(100).astype(np.float32)
    x1 = x[:50]
    x2 = x[50:]
    m = Mean()
    if gpu:
        x1_t = torch.from_numpy(x1).cuda()
        x2_t = torch.from_numpy(x2).cuda()
    else:
        x1_t = torch.from_numpy(x1)
        x2_t = torch.from_numpy(x2)
    m.update(x1_t)
    np.testing.assert_approx_equal(
        m.get(),
        np.mean(x1),
        significant=6,
    )
    # Test streaming functionality
    m.update(x2_t)
    np.testing.assert_approx_equal(
        m.get(),
        np.mean(x),
        significant=6,
    )


def test_Accuracy():
    import numpy as np
    from sklearn.metrics import accuracy_score
    x = np.random.randint(0, 2, (2, 100))
    labels, predictions = x
    labels1, predictions1 = x[:, :50]
    labels2, predictions2 = x[:, 50:]
    acc = Accuracy()
    acc.update(labels1, torch.from_numpy(predictions1))
    np.testing.assert_approx_equal(
        accuracy_score(labels1, predictions1),
        acc.get(),
        significant=6,
    )
    acc.update(torch.from_numpy(labels2), torch.from_numpy(predictions2))
    np.testing.assert_approx_equal(
        accuracy_score(labels, predictions),
        acc.get(),
        significant=6,
    )


def test_Accuracy_weights(gpu=False):
    import numpy as np
    from sklearn.metrics import accuracy_score
    x = np.random.randint(0, 2, (2, 100))
    labels, predictions = x
    labels1, predictions1 = x[:, :50]
    labels2, predictions2 = x[:, 50:]
    weights = np.random.random(100)
    weights1 = weights[:50]
    weights2 = weights[50:]
    labels1_t = torch.from_numpy(labels1)
    predictions1_t = torch.from_numpy(predictions1)
    weights1_t = torch.from_numpy(weights1)
    labels2_t = torch.from_numpy(labels2)
    predictions2_t = torch.from_numpy(predictions2)
    weights2_t = torch.from_numpy(weights2)
    if gpu:
        labels1_t = labels1_t.cuda()
        predictions1_t = predictions1_t.cuda()
        weights1_t = weights1_t.cuda()
        labels2_t = labels2_t.cuda()
        predictions2_t = predictions2_t.cuda()
        weights2_t = weights2_t.cuda()

    acc = Accuracy()
    acc.update(labels1_t, predictions1_t, weights1_t)
    np.testing.assert_approx_equal(
        accuracy_score(labels1, predictions1, sample_weight=weights1),
        acc.get(),
        significant=6,
    )
    acc.update(labels2_t, predictions2_t, weights2_t)
    np.testing.assert_approx_equal(
        accuracy_score(labels, predictions, sample_weight=weights),
        acc.get(),
        significant=6,
    )


def test_Precision():
    import numpy as np
    from sklearn.metrics import precision_score
    x = np.random.randint(0, 2, (2, 100))
    labels, predictions = x
    labels1, predictions1 = x[:, :50]
    labels2, predictions2 = x[:, 50:]
    weights = np.random.random(100)
    weights1 = weights[:50]
    weights2 = weights[50:]
    pr = Precision()
    pr.update(
        torch.from_numpy(labels1),
        torch.from_numpy(predictions1),
        torch.from_numpy(weights1),
    )
    np.testing.assert_approx_equal(
        precision_score(labels1, predictions1, sample_weight=weights1),
        pr.get(),
        significant=6,
    )
    pr.update(
        torch.from_numpy(labels2),
        torch.from_numpy(predictions2),
        torch.from_numpy(weights2),
    )
    np.testing.assert_approx_equal(
        precision_score(labels, predictions, sample_weight=weights),
        pr.get(),
        significant=6,
    )


def test_Recall():
    import numpy as np
    from sklearn.metrics import recall_score
    x = np.random.randint(0, 2, (2, 100))
    labels, predictions = x
    labels1, predictions1 = x[:, :50]
    labels2, predictions2 = x[:, 50:]
    weights = np.random.random(100)
    weights1 = weights[:50]
    weights2 = weights[50:]
    re = Recall()
    re.update(
        torch.from_numpy(labels1),
        torch.from_numpy(predictions1),
        torch.from_numpy(weights1),
    )
    np.testing.assert_approx_equal(
        recall_score(labels1, predictions1, sample_weight=weights1),
        re.get(),
        significant=6,
    )
    re.update(
        torch.from_numpy(labels2),
        torch.from_numpy(predictions2),
        torch.from_numpy(weights2),
    )
    np.testing.assert_approx_equal(
        recall_score(labels, predictions, sample_weight=weights),
        re.get(),
        significant=6,
    )


def test_F1Score():
    import numpy as np
    from sklearn.metrics import f1_score
    x = np.random.randint(0, 2, (2, 100))
    labels, predictions = x
    labels1, predictions1 = x[:, :50]
    labels2, predictions2 = x[:, 50:]
    weights = np.random.random(100)
    weights1 = weights[:50]
    weights2 = weights[50:]
    f1 = F1Score()
    f1.update(
        torch.from_numpy(labels1),
        torch.from_numpy(predictions1),
        torch.from_numpy(weights1),
    )
    np.testing.assert_approx_equal(
        f1_score(labels1, predictions1, sample_weight=weights1),
        f1.get(),
        significant=6,
    )
    f1.update(
        torch.from_numpy(labels2),
        torch.from_numpy(predictions2),
        torch.from_numpy(weights2),
    )
    np.testing.assert_approx_equal(
        f1_score(labels, predictions, sample_weight=weights),
        f1.get(),
        significant=6,
    )


if __name__ == '__main__':
    test_Mean()
    test_Mean(gpu=True)
    test_Accuracy()
    test_Accuracy_weights()
    test_Accuracy_weights(gpu=True)
    test_Precision()
    test_Recall()
    test_F1Score()
